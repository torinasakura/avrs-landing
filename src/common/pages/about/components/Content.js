import React from 'react'
import { Row, Layout } from 'flex-layouts'
import { Text } from 'avrs-ui/src/text'
import Career from './Career'
import Requirements from './Requirements'

const Content = () => (
  <Row>
    <Layout basis='20px' />
    <Layout>
      <Text
        size='large'
        color='black400'
      >
        Что такое Aversis?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        «Aversis Systems» - интеллектуальная система,
        которая занимается глобальным распределением вычислительной сети и дает
        возможность сдать в аренду не используемые процессорные мощности.
        Мы создали эксклюзивную программу AVERS для привлечения ресурсов
        для развития и ускоренного роста Глобальной Распределенной Вычислительной Системы.
        Задача компании Aversis Systems объединить полученные ресурсы и распределить их далее,
        в соответствии с запросами. Вознаграждение, полученное компанией за предоставление вычислительного ресурса,
        распределяется между клиентами. Доходы начисляются пропорционально по объему
        и времени предоставления вычислительных ресурсов, согласно тарифу.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-what-you-need'
        size='large'
        color='black400'
      >
        Для чего это нужно?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Специалисты компании разработали программу,
        которая позволяет объединить неиспользуемые ресурсы персональных компьютеров
        по всему миру в единый суперкомпьютер. Его суммарной производственной мощности
        может хватить на обработку невероятного количества информации.
        Объединяя ежедневно тысячи компьютеров «Аверсис»  уже сегодня предоставляет
        ресурс в разработке новейших технологий, помогает решить сложные задачи в здравоохранении,
        осуществляет добычу крипто валют и поддерживает глобальные виртуальные ресурсы в интернете.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-advantages'
        size='large'
        color='black400'
      >
        Какие преимущества?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Компания Aversis Systems предлагает возможность получения дохода
        от сдачи в аренду свободного вычислительного ресурса своего компьютера,
        возможность получения реферального дохода (заработок идет к участнику проекта
        не из кошелька приглашенного пользователя, а из кошелька сервиса, в данном случае, Aversis Systems)
        от привлечения других участников в программу получения дохода от аренды вычислительного ресурса,
        возможность получения и использования эксклюзивных продуктов, предлагаемых компанией Aversis Systems.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-capabilities'
        size='large'
        color='black400'
      >
        Какие возможности?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Дайте возможность вашему компьютеру принести бо́льшую пользу
        По большому счету, практически каждый из нас не использует потенциал
        своего компьютера на полную мощность. На что он тратит свою энергию?
        А если тысячи домашних ПК объединить ...
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Таким образом, вычислительные ресурсы всех компьютеров участвуют во всех возможных
        и запрашиваемых сложных  вычислительных задачах, таких как добыча крипто валюты,
        поддержка игровых ресурсов, участие в научных исследованиях благодаря которым возможно
        разработать даже лекарство, например, от рака. Каждый компьютер получит свою и только СВОЮ задачу.
        Для этого специальный софт, устанавливаемый на ПК желающих участвовать в проекте,
        принимает небольшие задания, обрабатывает и отправляет полученные результаты обратно.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-target'
        size='large'
        color='black400'
      >
        Какая наша цель?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        По большому счету, практически каждый из нас не использует потенциал своего компьютера на полную мощность.
        Загляните под стол, где установлен компьютер. На что он тратит свой ресурс?
        На то, чтобы вы несколько раз в день нажали на несколько кнопок,
        ради встречи с «очередным одноклассником», или потусоваться «ВКонтакте». И это все?
        Ну, да, в холодное время года – компьютер может выступать как дополнительный источник
        подогрева воздуха под столом.  А ведь компьютер может принести неоценимую пользу.
        А если тысячи домашних компов объединить в одну систему и «заставить» работать по настоящему?
        Не удивляйтесь! Это все возможно! И, причем, с малыми затратами,
        но какая польза от этой совместной работы будет на выходе!
        Именно это и является нашей основной целью,
        предоставить каждому участнику возможность заработать на своем ПК
        присоединившись при этом к глобальной вычислительной Системе.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-join'
        size='large'
        color='black400'
      >
        Как стать участником?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Вам необходимо зарегистрироваться и приобрести любой пакет, предлагаемый Компанией.
        При оплате одного из пакетов, Вы получаете лицензию на использование
        специального программного обеспечения в системе по предоставлению вычислительного
        ресурса ПК и сдачу его в аренду Компании Aversis Systems.
      </Text>
    </Layout>

    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-income'
        size='large'
        color='black400'
      >
        Как мне получать доход?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        После регистрации Вы самостоятельно выбираете тариф и период
        на который вы готовы предоставить свой ресурс в аренду,
        оплачиваете лицензию для использования программного приложения.
        Устанавливаете приложение на своем компьютере и выполняете условия по выбранному тарифу,
        вознаграждение начнет начисляться вам автоматически.
        Контролировать процесс вы сможете на вашем компьютере или в личном кабинете или же в мобильном приложении.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-carrier'
        size='large'
        color='black400'
      >
        Условия карьеры
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Условием получения следующей позиции является продажа тарифов на суммы, которые стоят справа.
        Суммы суммируются также от ниже стоящих приглашенных каждому отдельно.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Career />
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Более того, в компании предусмотрены дополнительные премии для активных участников «AVERSIS».
        Например, Вы активно распространяете:
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        – Премия  «SUPER SELLER». Выплачивается партнеру,
        достигшему позиции  AVERS SELLER Премия составляет 3% от суммы продаж.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        – Премия SUPER MANAGER Выплачивается партнеру,
        достигшему позиции AVERS MANAGER Премия составляет 5% от суммы продаж.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        – Премия PREMIUM BONUS выплачивается каждый раз,
        партнеру который совершил оборот продаж на сумму 100.000,00 Евро
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-invite-members'
        size='large'
        color='black400'
      >
        Приглашение участников
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Приглашайте участников с помощью нашей реферальной программы и
        зарабатывайте дополнительный пассивный доход до тех пор,
        пока приглашенный Вами клиент предоставляет ресурс своего ПК в аренду.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-products'
        size='large'
        color='black400'
      >
        Продукты компании
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        AVERSIS SOFTWARE
      </Text>
    </Layout>
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Основным продуктом компании является уникальное приложение,
        которое способно объединить ресурс всех пользователей  и распределить их далее в соответствии с запросами.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        AVERSIS MOBILE APPS
      </Text>
    </Layout>
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Для удобства пользованием нашим основным продуктом и
        осуществления постоянного контроля мы предлагаем нашим клиентам бесплатное мобильное приложение.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        AVERSIS PREPAID CARD
      </Text>
    </Layout>
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Для удобства расчета с нашими клиентами мы предлагаем  AVERSIS MASTERCARD или AVERSIS VISA CARD
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-requirements'
        size='large'
        color='black400'
      >
        Требования для ПК
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Для того, что бы вы могли стать нашим клиентом Вам необходим  ПК,
        который соответствует минимальным требованиям одного из наших тарифов.
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Requirements />
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-referal'
        size='large'
        color='black400'
      >
        Реферальная программа
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Суть нашей реферальной системы заключается в ее многоуровности.
        Это означает, что получать прибыль можно не только с клиентов,
        которых пригласили вы. Как только вы зарегистрировались в системе «AVERSIS»
        вы становитесь в позицию AVERS STARTING и Вам предоставляется реферальная
        ссылка которую Вы можете посылать всем интересующимся нашей системой.
        И когда по вашей реферальной ссылке к системе подключается хотя бы один человек, вы начинаете зарабатывать.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-confidence'
        size='large'
        color='black400'
      >
        Почему я могу вам доверять?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Наша программа прошла проверку и зарегистрирована и опубликована в системе  Windows Store,
        что является гарантией того что наше приложение будет выполнять все задекларированные функции.
        Данную информацию вы можете получить непосредственно на сайте Microsoft.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-hack'
        size='large'
        color='black400'
      >
        Могут ли меня взломать?
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Сегодня очень велика опасность похищения логинов и паролей шпионскими программами
        и при помощи Бот-сетей. Или же с помощью всевозможных вирусов паролей,
        которые передают информации из вашего браузера запомненных паролей.
        Система Aversis заботится о том, чтобы вы не были взломаны за счёт двухэтапной авторизации.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-two-step-auth'
        size='large'
        color='black400'
      >
        Двухэтапная авторизация
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Для усиления безопасности мы применяем систему двухэтапной авторизации.
        При каждой авторизации наши пользователи получают дополнительный новый секретный код,
        что исключает любую возможность получения доступа злоумышленникам.
        То есть авторизоваться вы сможете только с использованием дополнительного одноразового кода,
        который придет вам на СМС.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-registration'
        size='large'
        color='black400'
      >
        Регистрация
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Зарегистрироваться на AVERSIS может любой желающий достигший 18-ти лет.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-conclusion-of-an-agreement'
        size='large'
        color='black400'
      >
        Заключение соглашения
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Заключить соглашение может каждый зарегистрированный пользователь, который намерен оплатить лицензию
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-termination'
        size='large'
        color='black400'
      >
        Расторжение соглашения
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Вы вправе расторгнуть соглашение с компанией и потребовать удаление данных в любое время.
        Для этого вам необходимо обратится в компанию с письмом уведомлением о том,
        что вы не желаете более сотрудничать с нашей компанией и желаете удалить все данные с нашего ресурса.
        Примечание:  При расторжении соглашения до срочно компания не возвращает деньги за оплаченный период лицензии.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-change-data'
        size='large'
        color='black400'
      >
        Изменение данных
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Вы можете всегда изменить свои личные данные для этого вам необходимо зайти
        в личный кабинет в раздел персональные данные.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-payment'
        size='large'
        color='black400'
      >
        Оплата лицензии
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Оплатить лицензию вы можете согласно выбранному тарифу в личном кабинете,
        любым предоставленным компанией способами.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <Text
        id='about-withdrawal'
        size='large'
        color='black400'
      >
        Вывод средств
      </Text>
    </Layout>
    <Layout basis='20px' />
    <Layout>
      <Text
        color='black400'
        weight='light'
        lineHeight='large'
      >
        Вы вправе вывести от  30,00 Евро все ваши заработанные средства
        в любое время любым имеющимся в компании способом.
      </Text>
    </Layout>
    <Layout basis='40px' />
    <Layout>
      <div style={{ padding: 30, background: '#F8FAFD' }}>
        <Row>
          <Layout>
            <Text
              size='large'
              color='black400'
            >
              Остались вопросы?
            </Text>
          </Layout>
          <Layout basis='10px' />
          <Layout>
            <Text
              color='black400'
              weight='light'
              lineHeight='large'
            >
              Для решения проблем и получения ответов вы можете
              обратиться через форму обратной связи в личном кабинете.
            </Text>
          </Layout>
        </Row>
      </div>
    </Layout>
    <Layout basis='80px' />
  </Row>
)

export default Content
